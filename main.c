 /*
 * MAIN Generated Driver File
 * 
 * @file main.c
 * 
 * @defgroup main MAIN
 * 
 * @brief This is the generated driver implementation file for the MAIN driver.
 *
 * @version MAIN Driver Version 1.0.0
*/

/*
� [2023] Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip 
    software and any derivatives exclusively with Microchip products. 
    You are responsible for complying with 3rd party license terms  
    applicable to your use of 3rd party software (including open source  
    software) that may accompany Microchip software. SOFTWARE IS ?AS IS.? 
    NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS 
    SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,  
    MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT 
    WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY 
    KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF 
    MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE 
    FORESEEABLE. TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP?S 
    TOTAL LIABILITY ON ALL CLAIMS RELATED TO THE SOFTWARE WILL NOT 
    EXCEED AMOUNT OF FEES, IF ANY, YOU PAID DIRECTLY TO MICROCHIP FOR 
    THIS SOFTWARE.
*/

/*
    Main application
*/

#include <avr/io.h>
#include <system.h>
#include "main.h"
#include "i2c.h"

#define I2C_BUFFER_SIZE 32
uint8_t i2c_read_buffer[I2C_BUFFER_SIZE] = {0};
uint8_t i2c_write_buffer[I2C_BUFFER_SIZE] = {0}; //TODO: Not needed for clients?

volatile board_t client_board = {
	.features = {
		.drive_pins = 0,
		.sense_pins = 0,
		.encoders = 0,
		.joysticks = 1,
		.special_switches = 1,
	},
};
volatile board_t *this_board = &client_board;

volatile ADC_MUXPOS_t adc_axis = ADC_MUXPOS_AIN5_gc;
volatile uint8_t joystick_calibrate = 1;
volatile uint16_t joystick_offset[2] = {0};

void joystick_handler(void);
void switch_handler(void);
bool i2c_handle_request(uint8_t request);
bool i2c_handle_command(uint8_t command);
bool i2c_handler(i2c_client_transfer_event_t clientEvent);

int main(void)
{
	SYSTEM_Initialize();

	ADC_ResultReadyCallbackRegister(joystick_handler);
	LUT0_OUT_SetInterruptHandler(switch_handler);
	I2C_Client.CallbackRegister(i2c_handler);

	ADC_StartConversion(adc_axis);

    while(1)
    {
		 // Everything is interrupt based!
    }    
}

void joystick_handler(void)
{
	// Read measurement
	uint16_t measurement = ADC_GetConversionResult();

	if (adc_axis == ADC_MUXPOS_AIN5_gc)
	{
		// Store X-axis
		if (joystick_calibrate == 1)
		{
			joystick_offset[0] = measurement;
			joystick_calibrate++;
		}
		else
		{
			client_board.joystick_array[0][0] = measurement - joystick_offset[0];		
		}

		// Switch axis
		adc_axis = ADC_MUXPOS_AIN6_gc;
	}
	else
	{
		// Store Y-axis
		if (joystick_calibrate > 1) // Skip if the calibrate flag was set during the Y-measurement
		{
			joystick_offset[1] = measurement;
			joystick_calibrate = 0;
		}
		else
		{
			client_board.joystick_array[0][1] = measurement - joystick_offset[1];

			// TODO: Only set flag on change
			client_board.status.joystick_flags = 0x01;
		}

		// Switch axis
		adc_axis = ADC_MUXPOS_AIN5_gc;	
	}
	
	// Start next measurement
	ADC_StartConversion(adc_axis);
}

void switch_handler(void)
{
	// update state and set flag
	if (SW0_GetValue())
	{
		client_board.specialswitches = 0x01;
	}
	else
	{
		client_board.specialswitches = 0x00;
	}
	
	client_board.status.specialswitch_flags |= 0x01;
}

bool i2c_handle_request(uint8_t request)
{
	bool status = true;
	
	switch (request)
	{
		case I2C_COMMAND_READ_SPECIAL:
		{
			I2C_Client.WriteByte(client_board.specialswitches);
			break;
		}
		case I2C_COMMAND_READ_JOYSTICK1_XL:
		case I2C_COMMAND_READ_JOYSTICK1_XH:
		case I2C_COMMAND_READ_JOYSTICK1_YL:
		case I2C_COMMAND_READ_JOYSTICK1_YH:
		{
			// TODO: 8- or 16-bit joystick reading?
			uint8_t *encoder_8bitptr = (uint8_t *)client_board.joystick_array;
			I2C_Client.WriteByte(encoder_8bitptr[request-I2C_COMMAND_READ_JOYSTICK1_XL]);
			if (request == I2C_COMMAND_READ_JOYSTICK1_XH)
			{
				client_board.joystick_array[0][0] = 0;
			}
			else if (request == I2C_COMMAND_READ_JOYSTICK1_YH)
			{
				client_board.joystick_array[0][1] = 0;
			}
			break;
		}
		case I2C_COMMAND_READ_FLAG0:
		case I2C_COMMAND_READ_FLAG1:
			I2C_Client.WriteByte(client_board.status.flags[request-I2C_COMMAND_READ_FLAG0]);
			client_board.status.flags[request-I2C_COMMAND_READ_FLAG0] = 0; // Clear flags after sending them
		break;
		case I2C_COMMAND_READ_FEATURE0:
		case I2C_COMMAND_READ_FEATURE1:
			I2C_Client.WriteByte(client_board.features.array[request-I2C_COMMAND_READ_FEATURE0]);
		break;
		default:
		status = false;
		break;
	}
	
	return status;
}

bool i2c_handle_command(uint8_t command)
{
	bool status = true;
	
	switch(command)
	{
		case I2C_COMMAND_RESET:
			_PROTECTED_WRITE(RSTCTRL.SWRR, RSTCTRL_SWRE_bm);
			break;
		case I2C_COMMAND_CALIBRATE:
			joystick_calibrate = 1;
			break;
		default:
//			status = false;
			break;
	}

	return status;
}

bool i2c_handler(i2c_client_transfer_event_t clientEvent)
{
	bool status = true;
	static volatile uint8_t command = I2C_COMMAND_NOTHING;
	static volatile uint8_t receive_cnt = 0;

	switch (clientEvent)
	{
		case I2C_CLIENT_TRANSFER_EVENT_RX_READY:
		{
			if (I2C_COMMAND_NOTHING == command)
			{
				// First byte is always a command from host
				command = I2C_Client.ReadByte();
			}
			else if (receive_cnt < I2C_BUFFER_SIZE)
			{
				// Receive more host data
				i2c_read_buffer[receive_cnt++] = I2C_Client.ReadByte();
			}
			else
			{
				// Received too much
				status = false;
			}
			break;
		}
		case I2C_CLIENT_TRANSFER_EVENT_TX_READY:
		{
			if (command < 0x80)
			{
				// Send requested data to the host
				status = i2c_handle_request(command);
				command++; // Post-increment command to allow for easy send of arrays
			}
			break;
		}
		case I2C_CLIENT_TRANSFER_EVENT_STOP_BIT_RECEIVED:
		{
			// Handle receive complete
			status = i2c_handle_command(command);

			// Reset command to prepare for new transfer
			command = I2C_COMMAND_NOTHING;
			receive_cnt = 0;
			break;
		}
		case I2C_CLIENT_TRANSFER_EVENT_ADDR_MATCH:
		// Not sure what to do with this one since will get repeated start when switching direction?
		break;
		case I2C_CLIENT_TRANSFER_EVENT_ERROR:
		// TODO: handle this
		default:
		{
			status = false;
			break;
		}
	}
	return status;
}
